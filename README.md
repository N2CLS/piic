# Reducing NIKA data with PIIC

## Initial steps

piic should be available on the cluster, provided that you added 

```
. /data/PHOTOM/shell/NIKA.bashrc
```
to your `$HOME/.bashrc`

## Patch PIIC

You need to patch a piic pro file in order for the script to work : 
in `pro/mapTPcreaModel.mopsic` replace the `red` occurence by `'outDir'`


## Retrieve or update scan lists : 
```
for source in GOODSNORTH COSMOS
do
    for array in 1 2 3
    do
        echo $source $array
        curl -s -o temp.LIST "https://n2cls.lam.fr/api/piic_scans/?sourcename=${source}&ar=${array}"
        python -c 'import json, sys; v=json.load(open("temp.LIST")); print("\n".join(str(x) for x in v))' > ${source}_ar${array}.LIST
    done
done
rm -f temp.LIST

for source in GOODSNORTH COSMOS
do 
   cat ${source}_ar1.LIST  ${source}_ar3.LIST > ${source}_ar13.LIST
done
```


## First iter loop on array 1

Setup the select files:

```
for source in GOODSNORTH COSMOS
do

cat <<EOT >> n2cls_select_${source}.piic

!!--------------------------- loading the list of observations and selecting ---------------------------

! del scan ...

!! Only for ar3
if (nikaBand.eq."3").or.(nikaBand.eq."13") then 

   ! del scan ...

end if

EOT

done
```
create the required directories
```
ln -s /data/PHOTOM/Raw/NIKAIMBFITS/ imbfitsDir
mkdir red stat png
```


launch a first iteration on `$source` (array 1)
```
srun -N 1 -c 1 --pty --x11 piic @ mapTP.piic 1 $source red
```

Update the corresponding bad scan list in `n2cls_select_${source}.piic`, hit `c` to continue. If the process hangs, one need to remove all corresponding files from `red/` `stat/` `png/` (for a given $source and $array : ):

```
rm -f red/${source}*ar${array}* stat/${source}*ar${array}* png/${source}*ar${array}*
```


When all the scans can be processed, for the first iteration, the process will hang on the polygon to define area for noise estimate. One needs to define a `mypolygon_${source}.poly` file following piic documentation.

You can then further go on iterations and on the last one the individuals maps will be created.

## Launch array2 / array3

For the moment the same list of bad scans apply for all arrays

you can add additionnal ban scans by array in the `n2cls_select_${source}.piic` file. And you can launch further arrays :

```
srun -N 1 -c 1 --pty --x11 piic @ mapTP.piic 2 $source red
srun -N 1 -c 1 --pty --x11 piic @ mapTP.piic 3 $source red
srun -N 1 -c 1 --pty --x11 piic @ mapTP.piic 13 $source red
```
